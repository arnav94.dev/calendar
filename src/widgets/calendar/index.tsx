import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import {CalendarExpander} from '../../components/calendar-expander';
import {CalendarList} from '../../components/calendar-list';
import {MonthCalendar} from '../../components/month-calendar';
import {MONTHS_ARRAY, TILE_SIZES} from '../../constants';

interface ICalendarProps {
  initialDate?: Date;
  onPressDate: Function;
}

export function Calendar(props: ICalendarProps) {
  const {initialDate, onPressDate} = props;
  const [date, setDate] = useState(
    initialDate ? initialDate.getDate() : new Date().getDate(),
  );
  const [month, setMonth] = useState(
    initialDate ? initialDate.getMonth() : new Date().getMonth(),
  );
  const [year, setYear] = useState(
    initialDate ? initialDate.getFullYear() : new Date().getFullYear(),
  );

  const [isFullView, setIsFullView] = useState(false);

  useEffect(() => {
    onPressDate(new Date(`${MONTHS_ARRAY[month]} ${date}, ${year}`));
  }, []);

  useEffect(() => {
    // if going back from January
    if (month < 0) {
      setMonth(11);
      setYear((year) => year - 1);
    }
    // if going forward from december
    if (month > 11) {
      setMonth(0);
      setYear((year) => year + 1);
    }
  }, [month]);

  useEffect(() => {
    onPressDate(new Date(`${MONTHS_ARRAY[month]} ${date}, ${year}`));
  }, [date, month, year]);

  return (
    <View style={{flex: 1}}>
      <CalendarExpander isFullView={isFullView} setIsFullView={setIsFullView} />
      <MonthCalendar
        size={TILE_SIZES.regular}
        date={date}
        setDate={setDate}
        month={month}
        year={year}
        isInteractive={true}
        setMonth={setMonth}
        setYear={setYear}
        isFullView={isFullView}
      />
      <CalendarList
        year={year}
        isFullView={isFullView}
        setIsFullView={setIsFullView}
      />
    </View>
  );
}
