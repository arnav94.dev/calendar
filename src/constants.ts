export const MONTHS_ARRAY = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const WEEK_DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export enum TILE_SIZES {
  small = 'small',
  regular = 'regular',
}

export const textSizes = {
  small: {
    title: 10,
    subtitle: 8,
    date: 10,
  },
  regular: {
    title: 14,
    subtitle: 10,
    date: 14,
  },
};

export const colors = {
  white: '#fff',
  black: '#000',
  overlay: 'rgba(0, 0, 0, 0.3)',
  grey: '#d0d0d0',
};

export const strings = {
  expand: 'View Full Calendar',
};
