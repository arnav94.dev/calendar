import {MONTHS_ARRAY} from './constants';

export const createYearCalendar = (year: number): number[] => {
  // Iterating over the array to get no of days in each month
  const calendar = MONTHS_ARRAY.map((item: string, index: number) => {
    let noOfDays;
    const isLeapYear =
      year % 4 === 0 &&
      ((year % 100 === 0 && year % 400 === 0) || year % 100 !== 0);
    const doesMonthHaveThirtyOneDays =
      (index <= 6 && index % 2 === 0) || (index > 6 && index % 2 !== 0);
    // calculation for number of days for each month
    if (index === 1) {
      // Leap year handling, for february
      isLeapYear ? (noOfDays = 29) : (noOfDays = 28);
    } else {
      // even indexed months have 31 days before  august, and odd indexed have 31 days after and including it
      doesMonthHaveThirtyOneDays ? (noOfDays = 31) : (noOfDays = 30);
    }
    return noOfDays;
  });
  // return an aray with no of days for each month
  return calendar;
};

export const createMonthCalendar = (month: number, year: number) => {
  // get no of days in each month
  const noOfDaysInMonth = createYearCalendar(year)[month];
  let dayOfTheMonth = 1;
  let week = 0;
  // create a 2-D matrix to populate with dates falling on weekdays
  // row index represents the week, column index represents the week day
  let calendar = [[0, 0, 0, 0, 0, 0, 0]];
  while (dayOfTheMonth <= noOfDaysInMonth) {
    const weekDayIndex = new Date(
      `${year}-${month + 1 > 9 ? month + 1 : `0${month + 1}`}-${
        dayOfTheMonth > 9 ? dayOfTheMonth : `0${dayOfTheMonth}`
      }`,
    ).getDay();
    calendar[week][weekDayIndex] = dayOfTheMonth;
    dayOfTheMonth++;
    if (weekDayIndex === 6) {
      week += 1;
      calendar.push([0, 0, 0, 0, 0, 0, 0]);
    }
  }
  return calendar;
};
