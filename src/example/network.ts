import {Alert} from 'react-native';
import {BASE_URL} from './constants';

export interface IData {
  _id?: string;
  name: string;
  eventName: string;
  startTime: string;
  endTime: string;
  date: string;
}

export interface IResponse {
  success: boolean;
  data?: IData[];
}

export const getEventsForDate = async (date: string) => {
  let response = await fetch(`${BASE_URL}/${date}`);
  const result: IResponse = await response.json();
  if (result && result.success && result.data) {
    return result.data;
  }
  Alert.alert('Error', 'Error fetching events for date');
};

export const postCalendarEvent = async (serverObj: IData) => {
  if (Object.values(serverObj).findIndex((item) => item === '') !== -1) {
    Alert.alert('Validation', 'Please fill all fields');
    return;
  }

  let response = await fetch(BASE_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      ...serverObj,
    }),
  });

  const result: IResponse = await response.json();
  if (result && result.success) {
    Alert.alert('Alert', 'Successfully Created Event.');
    return result.success;
  }
  Alert.alert('Alert', 'Event Creation Failed.');
};
