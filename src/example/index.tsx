import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Text,
  View,
  Pressable,
  Modal,
  TextInput,
  ActivityIndicator,
  Image,
} from 'react-native';
import {MONTHS_ARRAY} from '../constants';
import {Calendar} from '../widgets/calendar';
import {postCalendarEvent, IData, getEventsForDate} from './network';
import styles from './styles';

const close = require('../assets/cross.png');

export const Example = () => {
  const [date, setDate] = useState<string>('');
  const [events, setEvents] = useState<IData[]>([]);
  const [isVisible, setIsVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [formattedDateString, setFormattedDateString] = useState('');

  const openModal = () => {
    setIsVisible(true);
  };

  const closeModal = () => {
    setIsVisible(false);
  };

  const serverObj: IData = {
    name: '',
    eventName: '',
    startTime: '',
    endTime: '',
    date,
  };

  const getEvents = async () => {
    setIsLoading(true);
    const response = await getEventsForDate(date);
    if (response) {
      setEvents(response);
    }
    setIsLoading(false);
  };

  const submitForm = async () => {
    setEvents([]);
    const response = await postCalendarEvent(serverObj);
    if (response) {
      closeModal();
      setEvents([...events, serverObj]);
    }
  };

  useEffect(() => {
    getEvents();
  }, [date]);

  const onPressDate = (dateItem: Date) => {
    const dateString =
      dateItem.getDate() > 9 ? dateItem.getDate() : `0${dateItem.getDate()}`;
    const monthString =
      dateItem.getMonth() + 1 > 9
        ? dateItem.getMonth() + 1
        : `0${dateItem.getMonth() + 1}`;
    const yearString = dateItem.getFullYear();
    setDate(`${dateString}-${monthString}-${yearString}`);
    const formattedDate = new Date(
      `${yearString}-${monthString}-${dateString}`,
    );
    setFormattedDateString(
      `${formattedDate.getDate()} ${
        MONTHS_ARRAY[formattedDate.getMonth()]
      } ${yearString}`,
    );
  };

  const keyExtractor = (item: IData, index: number) =>
    item._id ?? `EVENT_ITEM-${index}`;

  const renderItem = ({item}: {item: IData}) =>
    !isLoading ? (
      <View style={styles.eventRow}>
        <View style={styles.bar} />
        <View style={styles.rightContainer}>
          <Text style={styles.time}>
            {item.startTime} - {item.endTime}
          </Text>
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.event}>{item.eventName}</Text>
        </View>
      </View>
    ) : (
      <View style={styles.loading}>
        <ActivityIndicator size="small" />
      </View>
    );

  return (
    <View style={styles.container}>
      <View style={styles.scrollContainer}>
        <FlatList
          data={events}
          ListHeaderComponent={
            <>
              <Calendar onPressDate={onPressDate} />
              <View style={styles.header}>
                <Text style={styles.date}>{formattedDateString}</Text>
                <Pressable style={styles.createEvent} onPress={openModal}>
                  <Text style={styles.addEvents}>Add Event</Text>
                </Pressable>
              </View>
            </>
          }
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      </View>
      <Modal
        transparent
        animationType="slide"
        visible={isVisible}
        onRequestClose={closeModal}>
        <View style={styles.modalContainer}>
          <View style={styles.formContainer}>
            <View style={styles.close}>
              <Text style={styles.formTitle}>Create Event</Text>
              <Pressable onPress={closeModal}>
                <Image
                  source={close}
                  resizeMode="contain"
                  style={{height: 24, width: 24}}
                />
              </Pressable>
            </View>
            <TextInput
              style={styles.textInput}
              placeholder={'Name'}
              onChangeText={(text) => (serverObj.name = text)}
            />
            <TextInput
              style={styles.textInput}
              placeholder={'Event Name'}
              onChangeText={(text) => (serverObj.eventName = text)}
            />
            <TextInput
              placeholder={'Start Time'}
              style={styles.textInput}
              onChangeText={(text) => (serverObj.startTime = text)}
            />
            <TextInput
              placeholder={'End Time'}
              style={styles.textInput}
              onChangeText={(text) => (serverObj.endTime = text)}
            />
            <Pressable style={styles.submitButton} onPress={submitForm}>
              <Text style={{fontWeight: 'bold'}}>Submit</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
};
