export const BASE_URL = 'https://lofty-gem-mechanic.glitch.me/api/calendar';

export const MONTHS_LIST = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
