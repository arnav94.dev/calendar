import React from 'react';
import {View, Pressable, Image} from 'react-native';
import {TILE_SIZES} from '../../constants';
import {MonthPicker} from '../month-picker';
import {YearPicker} from '../year-picker';
import styles from './styles';

const left = require('../../assets/left.png');
const right = require('../../assets/right.png');

interface IYearMonthAdjustmentSectionProps {
  isFullView: boolean;
  isInteractive: boolean;
  size: TILE_SIZES;
  year: number;
  setYear: Function;
  month: number;
  setMonth: Function;
}

export function MonthAdjustmentSection(
  props: IYearMonthAdjustmentSectionProps,
) {
  const {
    month,
    setMonth,
    year,
    setYear,
    size,
    isFullView,
    isInteractive,
  } = props;
  const decrementMonth = () => {
    setMonth((month: number) => month - 1);
  };

  const incrementMonth = () => {
    setMonth((month: number) => month + 1);
  };

  return (
    <View style={styles.container}>
      {!isFullView ? (
        <Pressable style={styles.arrowContainer} onPress={decrementMonth}>
          <Image source={left} resizeMode={'contain'} style={styles.arrow} />
        </Pressable>
      ) : null}
      <MonthPicker
        size={size}
        isInteractive={isInteractive}
        setMonth={setMonth}
        month={month}
      />
      <YearPicker
        size={size}
        setYear={setYear}
        year={year}
        isInteractive={isInteractive}
      />
      {!isFullView ? (
        <Pressable style={styles.arrowContainer} onPress={incrementMonth}>
          <Image source={right} resizeMode={'contain'} style={styles.arrow} />
        </Pressable>
      ) : null}
    </View>
  );
}
