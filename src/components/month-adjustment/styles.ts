import {StyleSheet} from 'react-native';
import {colors} from '../../constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  arrowContainer: {
    borderRadius: 12,
    height: 24,
    width: 24,
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrow: {height: 16, width: 16},
});
