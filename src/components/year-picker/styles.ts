import {StyleSheet} from 'react-native';
import {colors} from '../../constants';

export default StyleSheet.create({
  monthContainer: {
    padding: 8,
    margin: 8,
    backgroundColor: colors.grey,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  down: {height: 12, width: 12},
  listContainer: {
    flex: 1,
    backgroundColor: colors.overlay,
    justifyContent: 'center',
    alignItems: 'center',
  },
  list: {
    height: 300,
    width: 150,
    backgroundColor: colors.white,
    borderRadius: 8,
    overflow: 'hidden',
  },
  closeContainer: {
    position: 'absolute',
    bottom: 48,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    height: 48,
    width: 48,
    borderRadius: 24,
  },
  month: {margin: 8, padding: 8},
  cross: {height: 24, width: 24},
});
