import React, {useMemo, useState} from 'react';
import {Pressable, Text, Image, Modal, View, FlatList} from 'react-native';
import {textSizes, TILE_SIZES} from '../../constants';
import styles from './styles';

const down = require('../../assets/down.png');
const cross = require('../../assets/cross.png');

interface IYearPickerProps {
  year: number;
  setYear: Function;
  size: TILE_SIZES;
  isInteractive: boolean;
}

export function YearPicker(props: IYearPickerProps) {
  const {year, size, isInteractive, setYear} = props;
  const [isVisible, setIsVisible] = useState(false);

  // create an array of years from current year - 10 to current year + 10
  const data = useMemo(
    () =>
      new Array(21)
        .fill(new Date().getFullYear())
        .map((item: number, index: number) => item + index - 10),
    [],
  );

  const onYearPress = () => {
    isInteractive && setIsVisible(true);
  };

  const closeModal = () => {
    setIsVisible(false);
  };

  const keyExtractor = (item: number) => `YEAR_PICKER_${item}`;

  return (
    <>
      <Pressable style={styles.monthContainer} onPress={onYearPress}>
        <Text style={{fontSize: textSizes[size].title}}>{year}</Text>
        {isInteractive ? (
          <Image style={styles.down} resizeMode="contain" source={down} />
        ) : null}
      </Pressable>
      <Modal transparent onRequestClose={closeModal} visible={isVisible}>
        <View style={styles.listContainer}>
          <View style={styles.list}>
            <FlatList
              data={data}
              keyExtractor={keyExtractor}
              renderItem={({item}) => (
                <Pressable
                  style={styles.month}
                  onPress={() => {
                    setYear(item);
                    setIsVisible(false);
                  }}>
                  <Text>{item}</Text>
                </Pressable>
              )}
            />
          </View>
        </View>
        <Pressable onPress={closeModal} style={styles.closeContainer}>
          <Image source={cross} style={styles.cross} resizeMode={'contain'} />
        </Pressable>
      </Modal>
    </>
  );
}
