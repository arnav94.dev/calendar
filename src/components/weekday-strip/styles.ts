import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  stripContainer: {
    flexDirection: 'row',
  },
  weekDay: {textAlign: 'center', marginVertical: 8},
});
