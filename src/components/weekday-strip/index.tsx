import React from 'react';
import {View, Text} from 'react-native';
import {TILE_SIZES, WEEK_DAYS, textSizes} from '../../constants';
import styles from './styles';

interface IWeekDayStripProps {
  width: number;
  size: TILE_SIZES;
}

export function WeekDayStrip(props: IWeekDayStripProps) {
  const {width, size} = props;

  const tileWidth = width / 7;

  return (
    <View style={styles.stripContainer}>
      {WEEK_DAYS.map((item) => (
        <Text
          key={item}
          style={[
            styles.weekDay,
            {
              fontSize: textSizes[size].subtitle,
              width: tileWidth,
            },
          ]}>
          {item}
        </Text>
      ))}
    </View>
  );
}
