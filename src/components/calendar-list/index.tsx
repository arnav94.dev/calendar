import React from 'react';
import {Modal, View, Pressable, Image, FlatList} from 'react-native';
import {MONTHS_ARRAY, TILE_SIZES} from '../../constants';
import {MonthCalendar} from '../month-calendar';
import styles from './styles';

const cross = require('../../assets/cross.png');

interface ICalendarListProps {
  year: number;
  isFullView: boolean;
  setIsFullView: Function;
}

export function CalendarList(props: ICalendarListProps) {
  const {isFullView, setIsFullView, year} = props;

  const closeCalendarList = () => {
    setIsFullView(false);
  };

  return (
    <Modal
      visible={isFullView}
      animationType="slide"
      onRequestClose={closeCalendarList}>
      <View style={styles.container}>
        <Pressable style={styles.crossContainer} onPress={closeCalendarList}>
          <Image style={styles.cross} source={cross} />
        </Pressable>
        <FlatList
          data={MONTHS_ARRAY}
          numColumns={2}
          keyExtractor={(item) => item}
          renderItem={({index}) => (
            <View style={styles.calendar}>
              <MonthCalendar
                size={TILE_SIZES.small}
                month={index}
                year={year}
                isInteractive={false}
                isFullView={isFullView}
              />
            </View>
          )}
        />
      </View>
    </Modal>
  );
}
