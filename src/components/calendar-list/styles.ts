import {StyleSheet} from 'react-native';
import {colors} from '../../constants';

export default StyleSheet.create({
  container: {flex: 1, backgroundColor: colors.white},
  crossContainer: {flexDirection: 'row', alignSelf: 'flex-end'},
  cross: {height: 24, width: 24, margin: 14},
  calendar: {flex: 1, padding: 8},
});
