import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 8,
    borderRadius: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  up: {height: 32, width: 32},
  calendar: {height: 32, width: 32},
  label: {fontWeight: 'bold', textAlign: 'center'},
});
