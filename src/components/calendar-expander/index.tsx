import React from 'react';
import {Pressable, Image, Text} from 'react-native';
import styles from './styles';
import {strings} from '../../constants';

const calendar = require('../../assets/calendar.png');
const up = require('../../assets/up.png');

interface ICalendarExpanderProps {
  isFullView: boolean;
  setIsFullView: Function;
}

export function CalendarExpander(props: ICalendarExpanderProps) {
  const {isFullView, setIsFullView} = props;

  const toggleCalendarView = () => setIsFullView(!isFullView);

  return (
    <Pressable style={styles.container} onPress={toggleCalendarView}>
      <Image source={calendar} resizeMode={'contain'} style={styles.calendar} />
      <Text style={styles.label}>{strings.expand}</Text>
      <Image source={up} resizeMode={'contain'} style={styles.up} />
    </Pressable>
  );
}
