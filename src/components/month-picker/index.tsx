import React, {useState} from 'react';
import {FlatList, Image, Modal, Pressable, Text, View} from 'react-native';
import {TILE_SIZES, textSizes, MONTHS_ARRAY} from '../../constants';
import styles from './styles';

const down = require('../../assets/down.png');
const cross = require('../../assets/cross.png');

interface IMonthPickerProps {
  month: number;
  setMonth: Function;
  size: TILE_SIZES;
  isInteractive: boolean;
}

export function MonthPicker(props: IMonthPickerProps) {
  const {month, setMonth, size, isInteractive} = props;
  const [isVisible, setIsVisible] = useState(false);

  const onMonthPress = () => {
    isInteractive && setIsVisible(true);
  };

  const closeModal = () => {
    setIsVisible(false);
  };

  const keyExtractor = (item: string) => `MONTH_PICKER_${item}`;

  return (
    <>
      <Pressable style={styles.monthContainer} onPress={onMonthPress}>
        <Text style={{fontSize: textSizes[size].title}}>
          {MONTHS_ARRAY[month]}
        </Text>
        {isInteractive ? (
          <Image style={styles.down} resizeMode="contain" source={down} />
        ) : null}
      </Pressable>
      <Modal transparent onRequestClose={closeModal} visible={isVisible}>
        <View style={styles.listContainer}>
          <View style={styles.list}>
            <FlatList
              data={MONTHS_ARRAY}
              keyExtractor={keyExtractor}
              renderItem={({item, index}) => (
                <Pressable
                  style={styles.month}
                  onPress={() => {
                    setMonth(index);
                    setIsVisible(false);
                  }}>
                  <Text>{item}</Text>
                </Pressable>
              )}
            />
          </View>
        </View>
        <Pressable onPress={closeModal} style={styles.closeContainer}>
          <Image source={cross} style={styles.cross} resizeMode={'contain'} />
        </Pressable>
      </Modal>
    </>
  );
}
