import React, {useMemo, useState} from 'react';
import {Pressable, Text, View} from 'react-native';

import {colors, textSizes, TILE_SIZES} from '../../constants';
import {createMonthCalendar} from '../../utiils';
import {MonthAdjustmentSection} from '../month-adjustment';
import {WeekDayStrip} from '../weekday-strip';

interface IMonthCalendarProps {
  month: number;
  year: number;
  date: number;
  setDate: Function;
  isInteractive: boolean;
  size: TILE_SIZES;
  setMonth: Function;
  setYear: Function;
  isFullView: boolean;
}

export function MonthCalendar(props: IMonthCalendarProps) {
  const {
    date,
    month,
    year,
    setDate,
    isInteractive,
    size,
    setMonth,
    setYear,
    isFullView,
  } = props;

  const calendar = useMemo(() => createMonthCalendar(month, year), [
    month,
    year,
  ]);

  const [width, setWidth] = useState(0);

  // so that the calendar adapts to its container view
  const tileWidth = width / 7;

  return (
    <View onLayout={(e) => setWidth(e.nativeEvent.layout.width)}>
      <MonthAdjustmentSection
        size={size}
        year={year}
        setYear={setYear}
        month={month}
        setMonth={setMonth}
        isFullView={isFullView}
        isInteractive={isInteractive}
      />
      <WeekDayStrip size={size} width={width} />
      <View>
        {calendar.map((item, index) => (
          <View key={`ROW-${index}`} style={{flexDirection: 'row'}}>
            {item.map((itemInner, indexInner) => (
              <Pressable
                key={`DATE-${itemInner}-${indexInner}`}
                onPress={() => {
                  isInteractive && setDate(itemInner);
                }}
                style={{
                  height: tileWidth,
                  width: tileWidth,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    height: tileWidth * 0.8,
                    width: tileWidth * 0.8,
                    borderRadius: tileWidth * 0.4,
                    backgroundColor:
                      isInteractive && itemInner === date
                        ? colors.black
                        : colors.white,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: textSizes[size].date,
                      color:
                        isInteractive && itemInner === date
                          ? colors.white
                          : colors.black,
                    }}>
                    {itemInner === 0 ? '' : itemInner}
                  </Text>
                </View>
              </Pressable>
            ))}
          </View>
        ))}
      </View>
    </View>
  );
}
