# Calendar

Calendar widget build with react native

**Props**

| name        | required | type     | initial value |
| ----------- | -------- | -------- | ------------- |
| initialDate | no       | Date     | new Date()    |
| onPressDate | yes      | Function | undefined     |

![ScreenShot](https://i.imgur.com/SmGGmnm.png)

Example usage is located in `src/example`

**How to run**
1. run `yarn` in the root of the project
2. to run on android, run `yarn android`
3. to run on ios, open ios/intellect_calendar.xcworkspace in Xcode, and run from there.